/*
1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

Comando:
mongoimport -d students -c grades < grades.json

Respuesta:
2023-10-13T06:05:14.059-0600    connected to: mongodb://localhost/
2023-10-13T06:05:14.107-0600    800 document(s) imported successfully. 0 document(s) failed to import.

2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count? 800.

Comando:
mongo

Respuesta:
MongoDB shell version v5.0.21
connecting to: mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("36b42b53-e385-423b-bc5a-7ad7c1e5d8a2") }
MongoDB server version: 5.0.21
================
Warning: the "mongo" shell has been superseded by "mongosh",
which delivers improved usability and compatibility.The "mongo" shell has been deprecated and will be removed in
an upcoming release.
For installation instructions, see
https://docs.mongodb.com/mongodb-shell/install/
================
---
The server generated these startup warnings when booting:
        2023-10-13T05:48:49.467-06:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
---

Comando:
use students

Respuesta:
switched to db students

Comando:
db.grades.count()

Respuesta:
800

3) Encuentra todas las calificaciones del estudiante con el id numero 4.

Comando:
db.grades.find({student_id:4}, {_id:0, score:1})

Respuesta:
{ "score" : 87.89071881934647 }
{ "score" : 5.244452510818443 }
{ "score" : 28.656451042441 }
{ "score" : 27.29006335059361 }

4) ¿Cuántos registros hay de tipo exam? 200.

Comando:
db.grades.find({type:"exam"}).count()

Respuesta:
200

5) ¿Cuántos registros hay de tipo homework? 400.

Comando:
db.grades.find({type:"homework"}).count()

Respuesta:
400
*/
/*
6) ¿Cuántos registros hay de tipo quiz? 200.

Comando:
db.grades.find({type:"quiz"}).count()

Respuesta:
200

7) Elimina todas las calificaciones del estudiante con el id numero 3.

Comando:
db.grades.updateMany({"student_id":3},{$set:{"score":null}})

Respuesta:
{ "acknowledged" : true, "matchedCount" : 4, "modifiedCount" : 4 }

8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ? El 9.

Comando:
db.grades.find({ score:75.29561445722392 },{_id:0,student_id:1})

Respuesta:
{ "student_id" : 9 }

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

Comando:
db.grades.update({_id:ObjectId("50906d7fa3c412bb040eb591")},{$set:{"score":100}});

Respuesta:
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

10) A qué estudiante pertenece esta calificación. Al 6.

Comando:
db.grades.find({score:100},{_id:0,student_id:1})

Respuesta:
{ "student_id" : 6 }

*/
